let API_KEY = ''
let API_SECRET = ''
let CLOUD_NAME = ''
const CryptoJS = require('crypto-js');

const API_URL = 'https://api.cloudinary.com/v1_1/';

export function init(key, secret, cloud_name) {
  API_KEY = key
  API_SECRET = secret
  CLOUD_NAME = cloud_name
  UPLOAD_URL = API_URL + CLOUD_NAME +'/image/upload'
}

export function UploadImage (uri) {

  console.log(API_KEY, API_SECRET, CLOUD_NAME)
  let timestamp = (Date.now() / 1000 | 0).toString()
  let hash_string = 'timestamp=' + timestamp + API_SECRET
  let signature = CryptoJS.SHA1(hash_string).toString();

  let formdata = new FormData()
  formdata.append('file', {uri: uri, type: 'image/jpg', name: timestamp})
  formdata.append('timestamp', timestamp)
  formdata.append('api_key', API_KEY)
  formdata.append('signature', signature)

  const config = {
    method: 'POST',
    body: formdata
  }

  return fetch(UPLOAD_URL, config)
  .then(res => res.json())
  .then(res => {
    console.log(res)
    return res.url
  })
  .catch((err) => {
    console.log(err)
  })
}