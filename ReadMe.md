This is react native module of Image uploading to Cloudinary
You can simply using this

`step 1` : import RNCloudinary from 'react-native-cloudinary-x' </br>

`step 2` : RNCloudinary.init(API_KEY,API_SECRET, CLOUD_NAME) </br>

`step 3` : RNCloudinary.UploadImage(uri) </br>
    
this will return response primise <br>

this is example code </br>

```
import RNCloudinary from 'react-native-cloudinary-x'

RNCloudinary.init(API_KEY,API_SECRET, CLOUD_NAME)

RNCloudinary.UploadImage(uri).then((res) => {

}
.catch(err => {
}
)
```